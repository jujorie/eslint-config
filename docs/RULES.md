# Rules Aplied

## no-cond-assign

see [no-cond-assign](https://eslint.org/docs/rules/no-cond-assign) rule

```json
{
    "no-cond-assign": "off"
}
```

## no-irregular-whitespace

see [no-irregular-whitespace](https://eslint.org/docs/rules/no-irregular-whitespace) rule

```json
{
    "no-irregular-whitespace": "error"
}
```

## no-unexpected-multiline

see [no-unexpected-multiline](https://eslint.org/docs/rules/no-unexpected-multiline) rule

```json
{
    "no-unexpected-multiline": "error"
}
```

## valid-jsdoc

see [valid-jsdoc](https://eslint.org/docs/rules/valid-jsdoc) rule

```json
{
    "valid-jsdoc": [
        "error",
        {
            "requireParamDescription": false,
            "requireReturnDescription": false,
            "requireReturn": false,
            "prefer": {
                "returns": "return"
            }
        }
    ]
}
```

## curly

see [curly](https://eslint.org/docs/rules/curly) rule

```json
{
    "curly": [
        "error",
        "multi-line"
    ]
}
```

## guard-for-in

see [guard-for-in](https://eslint.org/docs/rules/guard-for-in) rule

```json
{
    "guard-for-in": "error"
}
```

## no-caller

see [no-caller](https://eslint.org/docs/rules/no-caller) rule

```json
{
    "no-caller": "error"
}
```

## no-extend-native

see [no-extend-native](https://eslint.org/docs/rules/no-extend-native) rule

```json
{
    "no-extend-native": "error"
}
```

## no-extra-bind

see [no-extra-bind](https://eslint.org/docs/rules/no-extra-bind) rule

```json
{
    "no-extra-bind": "error"
}
```

## no-invalid-this

see [no-invalid-this](https://eslint.org/docs/rules/no-invalid-this) rule

```json
{
    "no-invalid-this": "error"
}
```

## no-multi-spaces

see [no-multi-spaces](https://eslint.org/docs/rules/no-multi-spaces) rule

```json
{
    "no-multi-spaces": "error"
}
```

## no-multi-str

see [no-multi-str](https://eslint.org/docs/rules/no-multi-str) rule

```json
{
    "no-multi-str": "error"
}
```

## no-new-wrappers

see [no-new-wrappers](https://eslint.org/docs/rules/no-new-wrappers) rule

```json
{
    "no-new-wrappers": "error"
}
```

## no-throw-literal

see [no-throw-literal](https://eslint.org/docs/rules/no-throw-literal) rule

```json
{
    "no-throw-literal": "error"
}
```

## no-with

see [no-with](https://eslint.org/docs/rules/no-with) rule

```json
{
    "no-with": "error"
}
```

## prefer-promise-reject-errors

see [prefer-promise-reject-errors](https://eslint.org/docs/rules/prefer-promise-reject-errors) rule

```json
{
    "prefer-promise-reject-errors": "error"
}
```

## no-unused-vars

see [no-unused-vars](https://eslint.org/docs/rules/no-unused-vars) rule

```json
{
    "no-unused-vars": [
        "error",
        {
            "args": "none"
        }
    ]
}
```

## array-bracket-newline

see [array-bracket-newline](https://eslint.org/docs/rules/array-bracket-newline) rule

```json
{
    "array-bracket-newline": "off"
}
```

## array-bracket-spacing

see [array-bracket-spacing](https://eslint.org/docs/rules/array-bracket-spacing) rule

```json
{
    "array-bracket-spacing": [
        "error",
        "never"
    ]
}
```

## array-element-newline

see [array-element-newline](https://eslint.org/docs/rules/array-element-newline) rule

```json
{
    "array-element-newline": "off"
}
```

## block-spacing

see [block-spacing](https://eslint.org/docs/rules/block-spacing) rule

```json
{
    "block-spacing": [
        "error",
        "never"
    ]
}
```

## brace-style

see [brace-style](https://eslint.org/docs/rules/brace-style) rule

```json
{
    "brace-style": "error"
}
```

## camelcase

see [camelcase](https://eslint.org/docs/rules/camelcase) rule

```json
{
    "camelcase": [
        "error",
        {
            "properties": "never"
        }
    ]
}
```

## comma-dangle

see [comma-dangle](https://eslint.org/docs/rules/comma-dangle) rule

```json
{
    "comma-dangle": "off"
}
```

## comma-spacing

see [comma-spacing](https://eslint.org/docs/rules/comma-spacing) rule

```json
{
    "comma-spacing": "error"
}
```

## comma-style

see [comma-style](https://eslint.org/docs/rules/comma-style) rule

```json
{
    "comma-style": "error"
}
```

## computed-property-spacing

see [computed-property-spacing](https://eslint.org/docs/rules/computed-property-spacing) rule

```json
{
    "computed-property-spacing": "error"
}
```

## eol-last

see [eol-last](https://eslint.org/docs/rules/eol-last) rule

```json
{
    "eol-last": "error"
}
```

## func-call-spacing

see [func-call-spacing](https://eslint.org/docs/rules/func-call-spacing) rule

```json
{
    "func-call-spacing": "error"
}
```

## indent

see [indent](https://eslint.org/docs/rules/indent) rule

```json
{
    "indent": [
        "error",
        4
    ]
}
```

## key-spacing

see [key-spacing](https://eslint.org/docs/rules/key-spacing) rule

```json
{
    "key-spacing": "error"
}
```

## keyword-spacing

see [keyword-spacing](https://eslint.org/docs/rules/keyword-spacing) rule

```json
{
    "keyword-spacing": "error"
}
```

## linebreak-style

see [linebreak-style](https://eslint.org/docs/rules/linebreak-style) rule

```json
{
    "linebreak-style": "error"
}
```

## max-len

see [max-len](https://eslint.org/docs/rules/max-len) rule

```json
{
    "max-len": [
        "error",
        {
            "code": 80,
            "tabWidth": 2,
            "ignoreUrls": true
        }
    ]
}
```

## new-cap

see [new-cap](https://eslint.org/docs/rules/new-cap) rule

```json
{
    "new-cap": "error"
}
```

## no-array-constructor

see [no-array-constructor](https://eslint.org/docs/rules/no-array-constructor) rule

```json
{
    "no-array-constructor": "error"
}
```

## no-mixed-spaces-and-tabs

see [no-mixed-spaces-and-tabs](https://eslint.org/docs/rules/no-mixed-spaces-and-tabs) rule

```json
{
    "no-mixed-spaces-and-tabs": "error"
}
```

## no-multiple-empty-lines

see [no-multiple-empty-lines](https://eslint.org/docs/rules/no-multiple-empty-lines) rule

```json
{
    "no-multiple-empty-lines": [
        "error",
        {
            "max": 2
        }
    ]
}
```

## no-new-object

see [no-new-object](https://eslint.org/docs/rules/no-new-object) rule

```json
{
    "no-new-object": "error"
}
```

## no-tabs

see [no-tabs](https://eslint.org/docs/rules/no-tabs) rule

```json
{
    "no-tabs": "error"
}
```

## no-trailing-spaces

see [no-trailing-spaces](https://eslint.org/docs/rules/no-trailing-spaces) rule

```json
{
    "no-trailing-spaces": "error"
}
```

## object-curly-spacing

see [object-curly-spacing](https://eslint.org/docs/rules/object-curly-spacing) rule

```json
{
    "object-curly-spacing": "error"
}
```

## one-var

see [one-var](https://eslint.org/docs/rules/one-var) rule

```json
{
    "one-var": [
        "error",
        {
            "var": "never",
            "let": "never",
            "const": "never"
        }
    ]
}
```

## padded-blocks

see [padded-blocks](https://eslint.org/docs/rules/padded-blocks) rule

```json
{
    "padded-blocks": [
        "error",
        "never"
    ]
}
```

## quote-props

see [quote-props](https://eslint.org/docs/rules/quote-props) rule

```json
{
    "quote-props": [
        "error",
        "consistent"
    ]
}
```

## quotes

see [quotes](https://eslint.org/docs/rules/quotes) rule

```json
{
    "quotes": [
        "error",
        "single",
        {
            "allowTemplateLiterals": true
        }
    ]
}
```

## require-jsdoc

see [require-jsdoc](https://eslint.org/docs/rules/require-jsdoc) rule

```json
{
    "require-jsdoc": [
        "error",
        {
            "require": {
                "FunctionDeclaration": true,
                "MethodDefinition": true,
                "ClassDeclaration": true
            }
        }
    ]
}
```

## semi

see [semi](https://eslint.org/docs/rules/semi) rule

```json
{
    "semi": "error"
}
```

## semi-spacing

see [semi-spacing](https://eslint.org/docs/rules/semi-spacing) rule

```json
{
    "semi-spacing": "error"
}
```

## space-before-blocks

see [space-before-blocks](https://eslint.org/docs/rules/space-before-blocks) rule

```json
{
    "space-before-blocks": "error"
}
```

## space-before-function-paren

see [space-before-function-paren](https://eslint.org/docs/rules/space-before-function-paren) rule

```json
{
    "space-before-function-paren": [
        "error",
        {
            "asyncArrow": "always",
            "anonymous": "never",
            "named": "never"
        }
    ]
}
```

## spaced-comment

see [spaced-comment](https://eslint.org/docs/rules/spaced-comment) rule

```json
{
    "spaced-comment": [
        "error",
        "always"
    ]
}
```

## switch-colon-spacing

see [switch-colon-spacing](https://eslint.org/docs/rules/switch-colon-spacing) rule

```json
{
    "switch-colon-spacing": "error"
}
```

## arrow-parens

see [arrow-parens](https://eslint.org/docs/rules/arrow-parens) rule

```json
{
    "arrow-parens": [
        "error",
        "always"
    ]
}
```

## constructor-super

see [constructor-super](https://eslint.org/docs/rules/constructor-super) rule

```json
{
    "constructor-super": "error"
}
```

## generator-star-spacing

see [generator-star-spacing](https://eslint.org/docs/rules/generator-star-spacing) rule

```json
{
    "generator-star-spacing": [
        "error",
        "after"
    ]
}
```

## no-new-symbol

see [no-new-symbol](https://eslint.org/docs/rules/no-new-symbol) rule

```json
{
    "no-new-symbol": "error"
}
```

## no-this-before-super

see [no-this-before-super](https://eslint.org/docs/rules/no-this-before-super) rule

```json
{
    "no-this-before-super": "error"
}
```

## no-var

see [no-var](https://eslint.org/docs/rules/no-var) rule

```json
{
    "no-var": "error"
}
```

## prefer-const

see [prefer-const](https://eslint.org/docs/rules/prefer-const) rule

```json
{
    "prefer-const": [
        "error",
        {
            "destructuring": "all"
        }
    ]
}
```

## prefer-rest-params

see [prefer-rest-params](https://eslint.org/docs/rules/prefer-rest-params) rule

```json
{
    "prefer-rest-params": "error"
}
```

## prefer-spread

see [prefer-spread](https://eslint.org/docs/rules/prefer-spread) rule

```json
{
    "prefer-spread": "error"
}
```

## rest-spread-spacing

see [rest-spread-spacing](https://eslint.org/docs/rules/rest-spread-spacing) rule

```json
{
    "rest-spread-spacing": "error"
}
```

## yield-star-spacing

see [yield-star-spacing](https://eslint.org/docs/rules/yield-star-spacing) rule

```json
{
    "yield-star-spacing": [
        "error",
        "after"
    ]
}
```
