const {existsSync, writeFile} = require('fs');
const {resolve} = require('path');
const configBasename = '.eslintrc.json';


const target = resolve('..', '..', '..', configBasename);

if (existsSync(target)) {
    console.log('ESLINT Config found. Skipping');
    process.exit(0);
};

const config = {
    'extends': '@jujorie/eslint-config',
    'parserOptions': {
        'ecmaVersion': 2018
    },
    'env': {
        'node': true,
        'browser': true
    }
};

console.log('Create ESLINT config');
writeFile(
    target,
    JSON.stringify(config, null, 4),
    (err) => {
        if (err) {
            console.log(err.message);
            process.exit(1);
        } else {
            console.log('CONFIG DONE!');
        };
    }
);
