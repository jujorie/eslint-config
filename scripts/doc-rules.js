const {rules} = require('../index');
const {resolve} = require('path');
const {writeFile} = require('fs');

const lines = [
    '# Rules Aplied',
];

Object.keys(rules)
    .forEach((rule) => {
        const code = {};
        code[rule] = rules[rule];
        lines.push(...[
            '',
            `## ${rule}`,
            '',
            `see [${rule}](https://eslint.org/docs/rules/${rule}) rule`,
            '',
            '```json',
            JSON.stringify(code, null, 4),
            '```'
        ]);
    });

lines.push('');

writeFile(
    resolve(__dirname, '..', 'docs', 'RULES.md'),
    lines.join('\n'),
    (err) => {
        console.log('RULES.md writed');
    }
);
