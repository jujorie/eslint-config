describe('Test rules', () => {
    const {CLIEngine} = require('eslint');
    const config = require('../');

    const files = [
        'index.js',
        __filename
    ];

    const options = {
        useEslintrc: false,
        envs: ['node', 'es6'],
        parserOptions: {ecmaVersion: 2018},
        rules: config.rules,
    };

    it('Must pass all rules', () => {
        const report = new CLIEngine(options).executeOnFiles(files);
        expect(report.errorCount).toBe(0, 'Errors');
        expect(report.warningCount).toBe(0, 'Warnings');
        files.forEach((file, index) => {
            expect(report.results[index].filePath.endsWith(file)).toBeTruthy;
        });
    });
});

