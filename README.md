# eslint-config

Jujorie custom _eslint_ rules.

It follows the eslint [rules](http://eslint.org/docs/rules/)

## Contents

* [Commands](#commands)
* [Rules](#rules)

## Commands

### Create Rules Doc

Create the __docs/RULES.md__ file

```bash
npm run doc:rules
```

## Rules

See [here](./docs/RULES.md)
